FROM python:3.7-alpine

RUN mkdir /app_meta
COPY requirements.txt /app_meta
RUN apk add --update --no-cache py-pip build-base linux-headers \
&& pip install -r /app_meta/requirements.txt \
&& apk del build-base linux-headers py-pip \
&& rm -rf /var/cache/apk/* \
&& find / -name "*.pyc" -delete && find / -name "*.o" -delete

RUN mkdir /app
COPY source/ /app
WORKDIR /app

CMD ["python3","main.py"]

