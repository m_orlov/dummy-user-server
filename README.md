# Dummy user server
Server with lots of mistakes to check QA skills
## Launch
Can be started directly:   
`python3 <path_to_project>/source/main.py [port]`  
Or can be started in Docker container: 
```shell script
cd <path_to_project> 
docker build . -t <any_image_name> -f ./build.dockerfile
docker run -p <port>:80 <any_image_name>
# example
cd dummy_user_server
docker build . -t dummy_user_server -f ./build.dockerfile
docker run -p 80:80 dummy_user_server
```

## Methods
### Register
Creates new user  
URL: `POST http://{server_address}/register`  
Requires fields:
- login (str)
- password (str)
##### example
Request body:
```json
{
    "login": "test_login",
    "password": "test_password"
}
```
Response body:
```json
{
    "done": true
}
```

### Login
Login user, returns user token  
URL: `POST http://{server_address}/login`  
Requires fields:
- login (str)
- password (str)
##### example
Request body:
```json
{
    "login": "test_login",
    "password": "test_password"
}
```
Response body:
```json
{
    "done": true,
    "token": "eyJsb2dpbiI6ICJ0ZXN0X2xvZ2luIiwgInBhc3N3b3JkIjogInRlc3RfcGFzc3dvcmQifQ=="
}
```

### Get
Get user login by token  
URL: `GET http://{server_address}/get`  
Requires fields:
- token (str)  
##### example
Request:  
`GET localhost/get?token=eyJsb2dpbiI6ICJ0ZXN0X2xvZ2luIiwgInBhc3N3b3JkIjogInRlc3RfcGFzc3dvcmQifQ==`

Response body:
```json
{
    "done": true,
    "login": "test_login"
}
```

### Find
Find users with substring in login  
URL: `GET http://{server_address}/find`  
Requires fields:
- login (str)
- token (str)  
##### example
Request:  
`GET localhost/find?login=test&token=eyJsb2dpbiI6ICJ0ZXN0X2xvZ2luIiwgInBhc3N3b3JkIjogInRlc3RfcGFzc3dvcmQifQ==`

Response body:
```json
{
    "done": true,
    "users": [
        "test_login"
    ]
}
```

### Delete
Delete user by login  
Can be only done with user's own token  
URL: `POST http://{server_address}/delete`  
Requires fields:
- login (str)
- token (str)
##### example
Request body:
```json
{
    "login": "test_login",
    "token": "eyJsb2dpbiI6ICJ0ZXN0X2xvZ2luIiwgInBhc3N3b3JkIjogInRlc3RfcGFzc3dvcmQifQ=="
}
```
Response body:
```json
{
    "done": true
}
