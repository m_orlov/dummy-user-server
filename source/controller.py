import base64
import dataclasses
import json
from dataclasses import dataclass
from typing import Tuple, Dict

from aiohttp import web


class Controller:
    @dataclass
    class User:
        login: str
        password: str

    def __init__(self):
        self.login_to_user: Dict[str, Controller.User] = {}
        self.token_to_login: Dict[str, str] = {"root": "root"}

    async def register(self, request: web.Request) -> web.Response:
        login, password = await self._get_login_and_password_from_request(request)
        user = Controller.User(login, password)
        self.login_to_user[login] = user
        return web.json_response({'done': True})

    async def login(self, request: web.Request) -> web.Response:
        login, password = await self._get_login_and_password_from_request(request)
        user = await self._get_user_by_login_from_request(request, login)
        if password != user.password:
            raise web.HTTPBadRequest(reason=f"wrong password: '{password}'")
        token = base64.b64encode(json.dumps(dataclasses.asdict(user)).encode('utf8')).decode('utf8')
        self.token_to_login[token] = login
        return web.json_response({'done': True, 'token': token})

    async def get(self, request: web.Request) -> web.Response:
        token = request.query.get('token')
        if not token:
            raise web.HTTPBadRequest(reason="non empty token required")
        if token not in self.token_to_login:
            raise web.HTTPBadRequest(reason="invalid token")
        token_login = self.token_to_login[token]
        return web.json_response({'done': True, 'login': token_login})

    async def find(self, request: web.Request) -> web.Response:
        token = request.query.get('token')
        if not token:
            raise web.HTTPBadRequest(reason="non empty token required")
        if token not in self.token_to_login:
            raise web.HTTPBadRequest(reason="invalid token")
        login_substr = request.query.get('login')
        if not login_substr:
            raise web.HTTPBadRequest(reason="non empty login required")
        users = []
        for login, user in self.login_to_user.items():
            if login_substr in login:
                users.append(user.login)
        return web.json_response({'done': True, 'users': users})

    async def delete(self, request: web.Request) -> web.Response:
        target_login, token = await self._get_login_and_token_from_request(request)
        if token not in self.token_to_login:
            raise web.HTTPBadRequest(reason="invalid token")
        token_login = self.token_to_login[token]
        del self.login_to_user[target_login]
        if target_login != token_login:
            raise web.HTTPBadRequest(reason=f"cannot delete other users")
        return web.json_response({'done': True})

    async def _get_payload_from_post_request(self, request: web.Request):
        if request.body_exists and request.can_read_body:
            try:
                return await request.json()
            except json.JSONDecodeError:
                raise web.HTTPBadRequest(reason="json parse failed")
        else:
            raise web.HTTPBadRequest(reason="request body required")

    async def _check_token(self, request: web.Request) -> str:
        payload = await self._get_payload_from_post_request(request)
        token = payload.get('token')
        if not token:
            raise web.HTTPBadRequest(reason="non empty token required")
        if token not in self.token_to_login:
            raise web.HTTPBadRequest(reason="invalid token")
        return self.token_to_login[token]

    async def _get_login_and_password_from_request(self, request: web.Request) -> Tuple[str, str]:
        payload = await self._get_payload_from_post_request(request)
        login = payload.get('login')
        password = payload.get('password')
        if not login or not password:
            raise web.HTTPBadRequest(reason="non empty login and password required")
        return login, password

    async def _get_login_and_token_from_request(self, request: web.Request) -> Tuple[str, str]:
        payload = await self._get_payload_from_post_request(request)
        login = payload.get('login')
        token = payload.get('token')
        if not login or not token:
            raise web.HTTPBadRequest(reason="non empty login and token required")
        return login, token

    async def _get_login_from_request(self, request: web.Request) -> str:
        payload = await self._get_payload_from_post_request(request)
        login = payload.get('login')
        if not login:
            raise web.HTTPBadRequest(reason="non empty login")
        return login

    async def _get_user_by_login_from_request(self, request: web.Request, login: str=None) -> User:
        if login is None:
            login = await self._get_login_from_request(request)
        if login not in self.login_to_user:
            raise web.HTTPBadRequest(reason=f"no user found by login: '{login}'")
        return self.login_to_user[login]
