import sys

if sys.version_info[0] != 3 or sys.version_info[1] < 7:
    print(f"REQUIRED PYTHON 3.7, got: {'.'.join(map(str, sys.version_info[:2]))}")
    exit(-1)

from aiohttp import web

from controller import Controller

controller = Controller()
app = web.Application()

app.router.add_post('/register', controller.register)
app.router.add_post('/login', controller.login)
app.router.add_get('/get', controller.get)
app.router.add_get('/find', controller.find)
app.router.add_post('/delete', controller.delete)

port = 80
if len(sys.argv) > 1:
    port_str = sys.argv[1]
    try:
        port = int(port_str)
    except:
        print(f'FAILED TO CAST "{port_str}" as port number, usage:\npython3 {sys.argv[0]} [port]')
        exit(-1)


web.run_app(app, host='0.0.0.0', port=port, print=lambda *x, **y: print(*x, flush=True))
